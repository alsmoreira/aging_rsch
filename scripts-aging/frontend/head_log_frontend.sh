#!/bin/bash

# Script que gera cabecalhos nos arquivos de monitoramento
# Author: Anderson Moreira - alsm4@cin.ufpe.br

echo "%mem_residente  date time" >> /srv/cloud/one/logs/front/monitoramento-mem-res-processo-oned.txt
echo "%mem_virtual date time" >> /srv/cloud/one/logs/front/monitoramento-mem-virt-processo-oned.txt
echo "%ONED_cpu  date time" >> /srv/cloud/one/logs/front/monitoramento-cpu-processo-oned.txt
echo "%usr %sys %iowait %idle date time" >> /srv/cloud/one/logs/front/monitoramento-carga-cpu.txt
echo "%DISK_uso %por date" >> /srv/cloud/one/logs/front/monitoramento-disco.txt
echo "%uso_swap date time" >> /srv/cloud/one/logs/front/monitoramento-swap.txt
echo "%num_zumbis data hora" >> /srv/cloud/one/logs/front/monitoramento-zumbis.txt
echo "%ONED_cpu ONED_memRes ONED_memVirt date time" >> /srv/cloud/one/logs/front/monitoramento-sched.txt
echo "%SC_cpu SC_memRes SC_memVirt date time" >> /srv/cloud/one/logs/front/monitoramento-vmm.txt
echo "%IM_cpu IM_mem_Res IM_mem_Virt date time" >> /srv/cloud/one/logs/front/monitoramento-im.txt
echo "used  cache  buffer  date time" >> /srv/cloud/one/logs/front/monitoramento-mem.txt
