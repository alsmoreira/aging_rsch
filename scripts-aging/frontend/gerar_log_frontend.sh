#!/bin/bash

# Script que roda o monitoramento no frontend
# Author: Anderson Moreira - alsm4@cin.ufpe.br

/srv/cloud/one/plugins/front/check_cpu.sh &
/srv/cloud/one/plugins/front/check_disco.sh &
/srv/cloud/one/plugins/front/check_mem_swap.sh &
/srv/cloud/one/plugins/front/check_zombies.sh &
/srv/cloud/one/plugins/front/check_sched.sh &
/srv/cloud/one/plugins/front/check_vmm.sh &
/srv/cloud/one/plugins/front/check_im.sh &
/srv/cloud/one/plugins/front/check_oned_cpu.sh &
/srv/cloud/one/plugins/front/check_oned_res.sh &
/srv/cloud/one/plugins/front/check_oned_virt.sh &
/srv/cloud/one/plugins/front/check_mem.sh &
