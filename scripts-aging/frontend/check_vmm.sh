#!/bin/bash
# Script para monitoramento do processo de gerencia de VM (one_vmm) no frontend
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Obtem o PID do processo eucalyptus-sc
   pid_list=`ps aux | grep one_vmm | grep oneadmin | awk '{print $2}'`
   pid=`echo $pid_list | cut -d" " -f1`

   # Obtem o uso da CPU(%)
   string2=`pidstat -u -h -p $pid -T ALL -r 5 1 | sed -n '4p' `
   cpu=`echo "$string2" |  awk '{print $6}'`

   #Obtendo o uso de VSZ (Memoria virtual em kilobytes)
   mem_virtual=`echo "$string2" |  awk '{print $10}'`


   #Obtendo o uso de RSS (Memoria residente em kilobytes)
   mem_resid=`echo "$string2" |  awk '{print $11}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
   echo $cpu $mem_resid $mem_virtual $data $hora>> /srv/cloud/one/logs/front/monitoramento-vmm.txt
