#!/bin/bash
# Script para monitoramento da memoria residente do processo oned
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Obtem o PID do processo eucalyptus-cloud
   pid_list=`ps aux | grep 'oned -f' | awk '{print $2}'`
   pid=`echo $pid_list | cut -d" " -f1`

   # Obtem o campo de interesse RSS(KB)
   mem=`cat /proc/$pid/stat | awk '{print $24*4}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
   echo $mem $data $hora>> /srv/cloud/one/logs/front/monitoramento-mem-res-processo-oned.txt
