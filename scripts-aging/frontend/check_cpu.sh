#!/bin/bash
# Script para monitoramento de uso da CPU
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

# Armazena todos os campos
   cpu=`mpstat 5 2 | grep all | sed -n '2p'`

   # Separa os campos de interesse em variaveis
   usr=`echo $cpu | cut -d\  -f4`
   sys=`echo $cpu | cut -d\  -f6`
   iowait=`echo $cpu | cut -d\  -f7`
   idle=`echo $cpu | cut -d\  -f12`

   #trunca o numero, simulando um arredondamento
   idle=`echo $idle | awk '{printf("%d",$0);}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   # O tempo corresponde ao retorno do mpstat,
   # portanto  o uso de cpu eh a media do ultimo minuto
   tempo=`date --rfc-3339=seconds`


   # Escreve no arquivo as informacoes da CPU
   echo $usr $sys $iowait $idle $tempo>> /srv/cloud/one/logs/front/monitoramento-carga-cpu.txt
