#!/bin/bash
# Script para reiniciar, matar e iniciar a VM

# Funcao que instancia 8 VMs da imagem ftp-server-img, com o tipo m1.xlarge
instanciaVMs(){
   i=0
   while [ $i -lt 8 ] 
   do 
    euca-run-instances -t m1.xlarge -z cluster-modcs -k admin emi-3FDE12D4
    i=`expr $i + 1`
   done
   sleep 10
   medeTempoBootVMs
}

matarVMs(){
   instancias=`euca-describe-instances | grep INSTANCE | awk '{print $2}'`
   euca-terminate-instances $instancias
}

reiniciarVMs(){
   instancias=`euca-describe-instances | grep INSTANCE | awk '{print $2}'`
   euca-reboot-instances $instancias   
}

medeTempoBootVMs(){
 booting=TRUE
 flag=TRUE
 while [ $booting = TRUE ]
 do
  tempoPrint=`date --rfc-3339=seconds`
  running=`euca-describe-instances | grep running | wc -l`
  if [ $running -gt 0 ]
  then
    if [ $flag = TRUE ]
    then
     echo "Pelo menos 1 VM foi iniciada: "$tempoPrint>>boot-vm.txt
     echo "Pelo menos 1 VM foi iniciada: "$tempoPrint
     flag=FALSE
    fi
  fi

  # Se nao houver maquinas pendentes e forem menos de 8 em execucao, instancie novamente as que faltam
  pending=`euca-describe-instances | grep pending | wc -l`
  if [ $pending -eq 0 ]
  then
    if [ $running -lt 8 ]
    then
      num=`expr 8 - $running`
      i=0
      while [ $i -lt $num ] 
      do 
       euca-run-instances -t m1.xlarge -z cluster-modcs -k admin emi-3FDE12D4
       i=`expr $i + 1`
      done
    else    
      booting=FALSE
      echo "Todas as VMs foram iniciadas: "$tempoPrint>>boot-vm.txt
      echo "Todas as VMs foram iniciadas: "$tempoPrint
    fi
  fi
  sleep 5
 done
}

# Obtem o tempo inicial da execucao do script, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempoPrint=`date --rfc-3339=seconds`
   tempoInicial=`date +%s`
   
   echo "Inicio: "$tempoPrint >> historico-vm.txt
   echo "Inicio - Execucao do script: "$tempoPrint
   instanciaVMs   

while [ True ]
do  
  tempoPrint=`date --rfc-3339=seconds`
  tempo=`date +%s`

  # REINICIAR TODAS AS VMS
  # Se o tempo atual for menor que o tempo inicial + 5 horas, REINICIA todas as VMs
  if [ $tempo -lt $(expr $tempoInicial + 18000) ]
    then  
       #Comando para reiniciar todas as VMs
       echo "Reinicio VMs.: "$tempoPrint >> historico-vm.txt
       reiniciarVMs
    fi

  # MATAR TODAS AS VMS
  # Se o tempo atual for maior que o tempo inicial + 5 horas, MATA todas as VMs
  if [ $tempo -ge $(expr $tempoInicial + 18000) ]
    then
       #Comando para MATAR todas as VMs
       echo "Kill VMs ....: "$tempoPrint >> historico-vm.txt
       matarVMs 

       sleep 300
       
       #Comando para iniciar todas as VMs
       echo "Start VMs ...: "$tempoPrint >> historico-vm.txt
       instanciaVMs
       tempoInicial=`date +%s`
    fi
  
  sleep 600
done
