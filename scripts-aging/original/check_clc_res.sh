#!/bin/bash
# Script para monitoramento da memoria residente do processo eucalyptus no Cloud Controller

   #pega o nome da maquina
   usuario=`pwd | cut -d/ -f3`

   # Escreve o cabeçalho de identificação dos dados
   echo "%mem_residente  date time" >> /home/$usuario/logs/monitoramento-mem-res-processo-clc.txt

   # Obtem o PID do processo eucalyptus-cloud
   pid=`ps aux | grep eucalyptus-cloud | grep "108 " | awk '{print $2}'`

   # Obtem o campo de interesse RSS(KB)
   mem=`cat /proc/$pid/stat | awk '{print $24*4}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
   echo $mem $data $hora>> /home/$usuario/logs/monitoramento-mem-res-processo-clc.txt
