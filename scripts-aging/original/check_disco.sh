#!/bin/bash
# Script para monitoramento da utilização do disco

usuario=`pwd | cut -d/ -f3`

# Escreve o cabeçalho de identificação dos dados
echo "uso %uso date time" >> /home/$usuario/logs/monitoramento-disco.txt
   
   # Armazena todos os campos 
   disco=`df -ml -P / | sed -n 2p`
   
   # Separa os campos de interesse em variaveis
   uso=`echo $disco | cut -d\  -f3`
   por=`echo $disco | cut -d\  -f5`

   data=`date`

   #trunca o numero, simulando um arredondamento
   por=`echo $por | awk '{printf("%d",$0);}'`

   # Escreve no arquivo as informações do disco
   echo $uso $por $data>> /home/$usuario/logs/monitoramento-disco.txt
  
