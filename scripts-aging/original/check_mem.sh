#!/bin/bash
# Script para monitoramento de MEMORIA

usuario=`pwd | cut -d/ -f3`

# Escreve o cabeçalho de identificação dos dados
echo "used  cache  buffer  date time" >> /home/$usuario/logs/monitoramento-mem.txt
   
   # Armazena os campos 
   used=`free | grep Mem | awk {'print $3'}`
   cache=`free | grep Mem | awk {'print $6'}`
   buffer=`free | grep Mem | awk {'print $7'}`
   
   data=`date --rfc-3339=seconds`

   # Escreve no arquivo as informações do disco
   echo $used $cache $buffer $data>> /home/$usuario/logs/monitoramento-mem.txt
  
