#!/bin/bash
# Script para monitoramento de SWAP

usuario=`pwd | cut -d/ -f3`

# Escreve o cabeçalho de identificação dos dados
echo "uso_swap date time" >> /home/$usuario/logs/monitoramento-swap.txt
   
   # Armazena todos os campos 
   uso=`free | grep Swap`
   
   # Separa os campos de interesse em variaveis
   uso=`echo $uso | cut -d\  -f3`
   
   data=`date`

   # Escreve no arquivo as informações do disco
   echo $uso $data>> /home/$usuario/logs/monitoramento-swap.txt

