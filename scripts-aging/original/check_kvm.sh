#!/bin/bash

# Script para monitoramento do KVM no Cloud Nodes

#pega o nome da maquina
 usuario=`pwd | cut -d/ -f3`
 ip=`ifconfig | sed -n '2 p' | awk '{print $2}' | cut -d: -f2`

 #Obtem a quantidade de VMs rodando no nó.
  string=`ssh cloud@192.168.0.1 "sudo euca_conf --list-nodes | grep '"$ip"' "`
  qtd_vm=`pidof kvm | wc -w`
  ini=0
  ini2=2
  ini3=7
# Para rodar até pegar os dados de todas as VMs deste nó  
while [ $qtd_vm -ne $ini ]
do

# Obtem o PID do processo kvm

pid=`pidof kvm | cut -d" " -f"$qtd_vm"`

# Obter o nome da maquina virtual

ini3=$(($ini3 + $ini2))
name_vm=`echo "$string" | cut -d" " -f"$ini3"` 


# Escreve o cabeçalho de identificação dos dados

echo "%KVM_cpu KVM_mem_Res KVM_mem_Virt date time" >> /home/$usuario/logs/$name_vm-monitoramento-kvm.txt


# Obtem o uso da CPU(%)

   string2=`pidstat -u -h -p $pid -T ALL -r 5 1 | sed -n '4p' `
   cpu=`echo "$string2" |  awk '{print $6}'`

   #Obtendo o uso de VSZ (Memoria virtual em kilobytes)

   mem_virtual=`echo "$string2" |  awk '{print $10}'`


   #Obtendo o uso de RSS (Memoria residente em kilobytes)

   mem_resid=`echo "$string2" |  awk '{print $11}'`


   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS

   tempo=`date --rfc-3339=seconds`


   # Separa data e hora do tempo obtido

   data=`echo $tempo | cut -d\  -f1`

   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

 
   # Escreve no arquivo as informações capturadas

     echo $cpu $mem_resid $mem_virtual $data $hora>> /home/$usuario/logs/$name_vm-monitoramento-kvm.txt

   qtd_vm=$(($qtd_vm - 1))

done
   
