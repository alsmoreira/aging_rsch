#!/bin/bash
# Script para monitoramento da memoria residente do processo eucalyptus no Node Controller

   #pega o nome da maquina
   usuario=`pwd | cut -d/ -f3`

   # Escreve o cabeçalho de identificação dos dados
   echo "%mem_residente date time" >> /home/$usuario/logs/monitoramento-mem-res-processo-nc.txt
  
   # Obtem o PID do processo eucalyptus
   pid=`ps aux | grep eucalyptus | grep "107 " | awk '{print $2}'`

   # Obtem o campo de interesse RSS(KB)
   mem=`pidstat -u -h -p $pid -T ALL -r 1 1 | sed -n '4p' | awk '{print $11}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
   echo $mem $data $hora>> /home/$usuario/logs/monitoramento-mem-res-processo-nc.txt
  
