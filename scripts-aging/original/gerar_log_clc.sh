#!/bin/bash

#script que roda o monitoramento na máquina local

/home/cloud/plugins/check_clc_res.sh &
/home/cloud/plugins/check_clc_virt.sh &
/home/cloud/plugins/check_clc_cpu.sh &
/home/cloud/plugins/check_cpu.sh &
/home/cloud/plugins/check_disco.sh &
/home/cloud/plugins/check_mem_swap.sh &
/home/cloud/plugins/check_zombies.sh &
/home/cloud/plugins/check_cc.sh &
/home/cloud/plugins/check_sc.sh &
/home/cloud/plugins/check_walrus.sh &
/home/cloud/plugins/check_mem.sh &
