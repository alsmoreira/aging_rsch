#!/bin/bash

#script que roda o monitoramento em todas as máquinas.

while [ True ]
do
ssh cloud@192.168.0.1 "/home/cloud/gerar_log_clc.sh" &
ssh cloud@192.168.0.2 "/home/cloud/gerar_log_nc.sh" &
ssh cloud@192.168.0.3 "/home/cloud/gerar_log_nc.sh" &
ssh cloud@192.168.0.4 "/home/cloud/gerar_log_nc.sh" &
ssh cloud@192.168.0.5 "/home/cloud/gerar_log_nc.sh" &

sleep 60
done
