#!/bin/bash

#script que roda o monitoramento na máquina local

/home/cloud/plugins/check_nc_res.sh &
/home/cloud/plugins/check_nc_virt.sh &
/home/cloud/plugins/check_cpu.sh &
/home/cloud/plugins/check_disco.sh &
/home/cloud/plugins/check_mem_swap.sh &
/home/cloud/plugins/check_nc_cpu.sh &
/home/cloud/plugins/check_zombies.sh &
/home/cloud/plugins/check_kvm.sh &
/home/cloud/plugins/check_mem.sh &

