#!/bin/bash
# Script para monitoramento do Walrus no Cloud Controller

 #pega o nome da maquina
 usuario=`pwd | cut -d/ -f3`

# Escreve o cabeçalho de identificação dos dados
echo "%Walrus_cpu Walrus_mem_Res Walrus_mem_Virt date time" >> /home/$usuario/logs/monitoramento-walrus.txt

   # Obtem o PID do processo walrus
   pid=`ps aux | grep walrus | grep root | awk '{print $2}'`

   # Obtem o uso da CPU(%)

   string2=`pidstat -u -h -p $pid -T ALL -r 5 1 | sed -n '4p' `
   cpu=`echo "$string2" |  awk '{print $6}'`

   #Obtendo o uso de VSZ (Memoria virtual em kilobytes)

   mem_virtual=`echo "$string2" |  awk '{print $10}'`


   #Obtendo o uso de RSS (Memoria residente em kilobytes)

   mem_resid=`echo "$string2" |  awk '{print $11}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
     echo $cpu $mem_resid $mem_virtual $data $hora>> /home/$usuario/logs/monitoramento-walrus.txt

