#!/bin/bash
# Script para monitoramento de processos zumbis

   #pega o nome da maquina
   usuario=`pwd | cut -d/ -f3`

   # Escreve o cabeçalho de identificação dos dados
   echo "num_zumbis data hora" >> /home/$usuario/logs/monitoramento-zumbis.txt

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`
   
   # Armazena somente os campos de interesse
   num=`ps aux | awk '{if ($8~"Z"){print $0}}' | wc -l`
   listaZumbis=`ps aux | awk '{if ($8~"Z"){print $0,"\n"}}'`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações do disco
   echo $num $data $hora>> /home/$usuario/logs/monitoramento-zumbis.txt
   echo $data $hora>> /home/$usuario/logs/monitoramento-lista-zumbis.txt
   echo $listaZumbis>> /home/$usuario/logs/monitoramento-lista-zumbis.txt
