#!/bin/bash
# Script para monitoramento de uso do disco
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Armazena todos os campos
   disco=`df -ml -P / | sed -n 2p`

   # Separa os campos de interesse em variaveis
   uso=`echo $disco | cut -d\  -f3`
   por=`echo $disco | cut -d\  -f5`
   data=`date`

   #trunca o numero, simulando um arredondamento
   por=`echo $por | awk '{printf("%d",$0);}'`

   # Escreve no arquivo as informações do disco
   echo $uso $por $data>> /srv/cloud/one/logs/no/monitoramento-disco.txt
