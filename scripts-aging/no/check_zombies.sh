#!/bin/bash
# Script para monitoramento de processos zumbis
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`

   # Armazena somente os campos de interesse
   num=`ps aux | awk '{if ($8~"Z"){print $0}}' | wc -l`
   listaZumbis=`ps aux | awk '{if ($8~"Z"){print $0,"\n"}}'`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações do disco
   echo $num $data $hora>> /srv/cloud/one/logs/no/monitoramento-zumbis.txt
   echo $data $hora>> /srv/cloud/one/logs/no/monitoramento-lista-zumbis.txt
   echo $listaZumbis>> /srv/cloud/one/logs/no/monitoramento-lista-zumbis.txt
