#!/bin/bash
# Script para monitoramento de MEMORIA
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Armazena os campos
   used=`free | grep Mem | awk {'print $3'}`
   cache=`free | grep Mem | awk {'print $6'}`
   buffer=`free | grep Mem | awk {'print $7'}`

   data=`date --rfc-3339=seconds`

   # Escreve no arquivo as informacoes do disco
   echo $used $cache $buffer $data>> /srv/cloud/one/logs/no/monitoramento-mem.txt

