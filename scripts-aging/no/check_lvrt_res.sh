#!/bin/bash
# Script para monitoramento da memoria residente do processo libvirt no node
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Obtem o PID do processo libvirt
   pid=`ps aux | grep libvirt | grep root | awk '{print $2}'`

   # Obtem o campo de interesse RSS(KB)
   mem=`pidstat -u -h -p $pid -T ALL -r 1 1 | sed -n '4p' | awk '{print $11}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   # Escreve no arquivo as informações capturadas
   echo $mem $data $hora>> /srv/cloud/one/logs/no/monitoramento-mem-res-processo-lvrt.txt
