#!/bin/bash
# Script de geracao dos cabecalhos de monitoramento
# Author: Anderson Moreira - alsm4@cin.ufpe.br

echo "%mem_residente date time" >> /srv/cloud/one/logs/no/monitoramento-mem-res-processo-lvrt.txt
echo "%mem_virtual date time" >> /srv/cloud/one/logs/no/monitoramento-mem-virt-processo-lvrt.txt
echo "%usr %sys %iowait %idle date time" >> /srv/cloud/one/logs/no/monitoramento-carga-cpu.txt
echo "uso %uso date time" >> /srv/cloud/one/logs/no/monitoramento-disco.txt
echo "uso_swap date time" >> /srv/cloud/one/logs/no/monitoramento-swap.txt
echo "%cpu  date time" >> /srv/cloud/one/logs/no/monitoramento-cpu-processo-lvrt.txt
echo "num_zumbis data hora" >> /srv/cloud/one/logs/no/monitoramento-zumbis.txt
echo "%KVM_cpu KVM_mem_Res KVM_mem_Virt date time" >> /srv/cloud/one/logs/no/monitoramento-kvm.txt
echo "used  cache  buffer  date time" >> /srv/cloud/one/logs/no/monitoramento-mem.txt
