#!/bin/bash

#script que roda o monitoramento na máquina local
# Author: Anderson Moreira - alsm4@cin.ufpe.br

/srv/cloud/one/plugins/no/check_lvrt_res.sh &
/srv/cloud/one/plugins/no/check_lvrt_virt.sh &
/srv/cloud/one/plugins/no/check_cpu.sh &
/srv/cloud/one/plugins/no/check_disco.sh &
/srv/cloud/one/plugins/no/check_no_swap.sh &
/srv/cloud/one/plugins/no/check_lvrt_cpu.sh &
/srv/cloud/one/plugins/no/check_zombies.sh &
/srv/cloud/one/plugins/no/check_kvm.sh &
/srv/cloud/one/plugins/no/check_mem.sh &

