#!/bin/bash
# Script para monitoramento de SWAP
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

   # Armazena todos os campos
   uso=`free | grep Swap`

   # Separa os campos de interesse em variaveis
   uso=`echo $uso | cut -d\  -f3`

   data=`date`

   # Escreve no arquivo as informações do disco
   echo $uso $data >> /srv/cloud/one/logs/no/monitoramento-swap.txt
