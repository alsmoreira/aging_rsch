#!/bin/bash

# Script para monitoramento do KVM nos nodes
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

#Obtem a quantidade de VMs rodando no node.
qtd_vm=`pidof kvm | wc -w`

#inicializa as vari�veis de armazenamento
  ini=0
  temp_cpu=0
  temp_virt=0
  temp_res=0

# Para executar at� pegar os dados de todas as VMs deste node

while [ $qtd_vm -ne $ini ]
do
    # Pega informacoes do primeiro pid
    pid=`pidof kvm | cut -d" " -f"$qtd_vm"`
    string2=`pidstat -u -h -p $pid -T ALL -r 5 1 | sed -n '4p' `

    # Obtem o uso da CPU(%)
    cpu=`echo "$string2" |  awk '{print $6}'`
    temp_cpu=`echo "$temp_cpu + $cpu" | bc`

    #Obtendo o uso de VSZ (Memoria virtual em kilobytes)
    mem_virtual=`echo "$string2" |  awk '{print $10}'`
    temp_virt=`echo "$temp_virt + $mem_virtual" | bc`

    #Obtendo o uso de RSS (Memoria residente em kilobytes)
    mem_resid=`echo "$string2" |  awk '{print $11}'`
    temp_res=`echo "$temp_res + $mem_resid" | bc`

    # Passa para proximo pid
    qtd_vm=$(($qtd_vm - 1))

done

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

   #Esscreve no arquivo as informações capturadas

   echo $temp_cpu $temp_res $temp_virt $data $hora>> /srv/cloud/one/logs/no/monitoramento-kvm.txt
