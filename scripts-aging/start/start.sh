#!/bin/bash
#script que roda o monitoramento em todas as m�quinas
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

while [ True ]
do

ssh oneadmin@192.168.125.20 "source ~/.bash_profile && /srv/cloud/one/plugins/front/gerar_log_frontend.sh" &
ssh oneadmin@192.168.125.21 "source ~/.bash_profile && /srv/cloud/one/plugins/no3/gerar_log_no.sh" &
ssh oneadmin@192.168.125.22 "source ~/.bash_profile && /srv/cloud/one/plugins/no2/gerar_log_no.sh" &
ssh oneadmin@192.168.125.23 "source ~/.bash_profile && /srv/cloud/one/plugins/no4/gerar_log_no.sh" &
ssh oneadmin@192.168.125.24 "source ~/.bash_profile && /srv/cloud/one/plugins/no5/gerar_log_no.sh" &

#aguarda 1 minuto e volta a repetir os scripts
sleep 60
done
