#!/bin/bash
# Script para reiniciar, matar e iniciar a VM
# Author: Anderson Moreira - alsm4@cin.ufpe.br
# Original: Rubens Matos

# Funcao que instancia 6 VMs da imagem sistema_big_15G.qcow2
instanciaVMs(){
   i=0
   while [ $i -lt 6 ]
   do 
    onevm create /srv/cloud/one/images/winxp.one
    i=`expr $i + 1`
   done
   sleep 20
   medeTempoBootVMs
}

matarVMs(){
   instancias=`onevm list | awk '{print $1}'`
   qtd_vmk=`echo $instancias | wc -w`
   qtd_vmk=$(($qtd_vmk - 1))
   inik=1
   while [ $inik -le $qtd_vmk ]
   do
       vtemp=`echo $instancias | cut -d" " -f"$inik"`
       onevm delete $vtemp
       inik=$(($inik + 1))
   done
}

reiniciarVMs(){
   qtd_vmk=`echo $instancias | wc -w`
   qtd_vmk=$(($qtd_vmk - 1))
   inik=1
   while [ $inik -le $qtd_vmk ]
   do
       vtemp=`echo $instancias | cut -d" " -f"$inik"`
       onevm resubmit $vtemp
       inik=$(($inik + 1))
   done
}

medeTempoBootVMs(){
 booting=TRUE
 flag=TRUE
 while [ $booting = TRUE ]
 do
  tempoPrint=`date --rfc-3339=seconds`
  running=`onevm list | grep runn | wc -l`
  if [ $running -gt 0 ]
  then
    if [ $flag = TRUE ]
    then
     echo "Pelo menos 1 VM foi iniciada: "$tempoPrint>> /srv/cloud/one/logs/boot-vm.txt
     echo "Pelo menos 1 VM foi iniciada: "$tempoPrint
     flag=FALSE
    fi
  fi

  # Se nao houver maquinas pendentes e forem menos de 6 em execucao, instancie novamente as que faltam
  pending=`onevm list | grep prol | wc -l`
  if [ $pending -eq 0 ]
  then
    if [ $running -lt 6 ]
    then
      num=`expr 6 - $running`
      i=0
      while [ $i -lt $num ] 
      do 
       onevm create /srv/cloud/one/images/winxp.one
       i=`expr $i + 1`
      done
    else    
      booting=FALSE
      echo "Todas as VMs foram iniciadas: "$tempoPrint>> /srv/cloud/one/logs/boot-vm.txt
      echo "Todas as VMs foram iniciadas: "$tempoPrint
    fi
  fi
  sleep 5
 done
}

# Obtem o tempo inicial da execucao do script, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempoPrint=`date --rfc-3339=seconds`
   tempoInicial=`date +%s`
   
   echo "Inicio: "$tempoPrint >> /srv/cloud/one/logs/historico-vm.txt
   echo "Inicio - Execucao do script: "$tempoPrint
   instanciaVMs   

while [ True ]
do  
  tempoPrint=`date --rfc-3339=seconds`
  tempo=`date +%s`

  # REINICIAR TODAS AS VMS
  # Se o tempo atual for menor que o tempo inicial + 5 horas, REINICIA todas as VMs
  if [ $tempo -lt $(expr $tempoInicial + 18000) ]
    then  
       #Comando para reiniciar todas as VMs
       echo "Reinicio VMs.: "$tempoPrint >> /srv/cloud/one/logs/historico-vm.txt
       reiniciarVMs
    fi

  # MATAR TODAS AS VMS
  # Se o tempo atual for maior que o tempo inicial + 5 horas, MATA todas as VMs
  if [ $tempo -ge $(expr $tempoInicial + 18000) ]
    then
       #Comando para MATAR todas as VMs
       echo "Kill VMs ....: "$tempoPrint >> /srv/cloud/one/logs/historico-vm.txt
       matarVMs 

       sleep 300
       
       #Comando para iniciar todas as VMs
       echo "Start VMs ...: "$tempoPrint >> /srv/cloud/one/logs/historico-vm.txt
       instanciaVMs
       tempoInicial=`date +%s`
    fi
  
  sleep 600
done
