#!/bin/bash
#Released under gpl v2 by Anderson Moreira
###Following Section for Ubuntu 10.04.3 only
##Removes
#sudo apt-get remove python-imaging
##Reinstalls
#sudo apt-get install python-tk python-numpy python-imaging-tk
##EMC DEPENDS
#sudo apt-get install libgnomeprint2.2-0 libgnomeprintui2.2-0 libreadline5 tk8.5 tcl8.5 bwidget
###End Ubuntu 10.04.03 Section

###LINUX-CNC UBUNTU 10.04.1 LIVE CD
sudo apt-get remove hostmot2-firmware-all emc2


##general:
sudo apt-get install cvs subversion build-essential
##rtai:
sudo apt-get install libtool automake libncurses5-dev
##comedi-lib:
sudo apt-get install bison flex
##comedi-calibrate:
sudo apt-get install libboost-dev libboost-python-dev libboost-program-options-dev libgsl0-dev
##scilab 4.4.1:
sudo apt-get install gfortran sablotron tcl8.5-dev tk8.5-dev xaw3dg-dev libpvm3 pvm-dev libgtkhtml2-dev libvte-dev ocaml-native-compilers
##modnum 4.2.2
sudo apt-get install libgtk2.0-dev libvte-dev libwebkit-dev
##qrtailab:
sudo apt-get install libqt4-dev libqwt5-qt4-dev qt4-qmake
##git
sudo apt-get install git-core

##Linux-CNC: UNCOMMENT FOR STOCK UBUNTU!
#echo deb http://www.linuxcnc.org/lucid lucid base emc2.4 > /tmp/linuxcnc.list
#echo deb-src http://www.linuxcnc.org/lucid lucid base emc2.4 >> /tmp/linuxcnc.list
#sudo mv /tmp/linuxcnc.list /etc/apt/sources.list.d/
#gpg --keyserver pgpkeys.mit.edu --recv-key 8F374FEF
#gpg -a --export 8F374FEF | sudo apt-key add -
#sudo apt-get update
#sudo apt-get install linux-headers-2.6.32-122 linux-headers-2.6.32-122-rtai linux-image-2.6.32-122-rtai
##END UNCOMMENT FOR STOCK UBUNTU

cd /usr/src && sudo apt-get source linux-image-2.6.32-122-rtai
sudo ln -s /usr/src/linux-2.6.32 /usr/src/linux
cd /usr/src/linux && sudo make oldconfig
sudo mkdir /usr/local/linux
sudo mkdir /usr/local/linux/comedi
##RTAI
cd /usr/src && sudo wget --no-check-certificate https://www.rtai.org/RTAI/rtai-3.8.1.tar.bz2
sudo tar xjvf rtai-3.8.1.tar.bz2
sudo ln -s /usr/src/rtai-3.8.1 /usr/src/rtai

###STOCK UBUNTU
##INSTALLATION INSTRUCTIONS
#echo "Ensure directories are correct:"
#echo "Installation: /usr/realtime-2.6.32-122-rtai"
#echo "Linux source tree: /usr/src/linux-headers-2.6.32-122-rtai"
#echo "Under Machine, choose number of CPUs (check running cat /proc/cpuinfo and verifying how many processors are listed)"
#sleep 20
#cd /usr/src/rtai && sudo make menuconfig
##END INSTALLATION INSTRUCTIONS

##COMPILE RTAI
#cd /usr/src/rtai && sudo make
#cd /usr/src/rtai && sudo make install
#sudo sed -i 's/\(PATH=\"\)/\1\/usr\/realtime-2.6.32-122-rtai\/bin:/' /etc/environment
##END COMPILE RTAI

##UPDATE PATH
#export PATH=/usr/realtime-2.6.32-122-rtai/bin:$PATH
##END PATH UPDATE
###END STOCK UBUNTU

###
##LOAD MODULES
#cd ~ && cat <<EOF> start_rtai
# changes made to do loading rtai modules
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_smi.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_hal.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_lxrt.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_fifos.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_sem.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_mbx.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_msg.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_netrpc.ko
#/sbin/insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_shm.ko
# end rtai modules setup
#EOF
#sudo mv ~/start_rtai /usr/local/bin/
#sudo chown root:root /usr/local/bin/start_rtai
#sudo chmod 755 /usr/local/bin/start_rtai
#cd /usr/local/bin && sudo ./start_rtai
##END LOAD MODULES
###

##GIT COMEDI
cd /usr/local/src
cd /usr/local/src && sudo git clone git://comedi.org/git/comedi/comedi.git
cd /usr/local/src && sudo git clone git://comedi.org/git/comedi/comedilib.git
cd /usr/local/src && sudo git clone git://comedi.org/git/comedi/comedi_calibrate.git
cd /usr/local/src && sudo git clone git://comedi.org/git/comedi/comedi-nonfree-firmware.git
##END GIT COMEDI

##COMEDILIB
cd /usr/local/src/comedilib && sudo sh autogen.sh
cd /usr/local/src/comedilib && sudo ./configure
cd /usr/local/src/comedilib && sudo make
cd /usr/local/src/comedilib && sudo make install
cd /usr/local/src/comedilib && sudo mkdir /usr/local/include/linux
##END COMEDILIB

##COMEDI
cd /usr/local/src/comedi && sudo sh autogen.sh
cd /usr/local/src/comedi && sudo ./configure --with-linuxdir=/usr/src/linux-headers-2.6.32-122-rtai --with-rtaidir=/usr/realtime-2.6.32-122-rtai
cd /usr/local/src/comedi && sudo make
cd /usr/local/src/comedi && sudo make install
cd /usr/local/src/comedi && sudo depmod -a
cd /usr/local/src/comedi && sudo make dev
cd /usr/local/src/comedi && sudo ldconfig
##END COMEDI

##COMEDI CALIBRATE
cd /usr/local/src/comedi_calibrate && sudo autoreconf -i -B m4
cd /usr/local/src/comedi_calibrate && sudo ./configure
cd /usr/local/src/comedi_calibrate && sudo make
cd /usr/local/src/comedi_calibrate && sudo make install
##END COMEDI CALIBRATE

###RTAI #2
sudo cp /usr/local/src/comedi/include/linux/comedi.h /usr/local/include/
sudo cp /usr/local/src/comedi/include/linux/comedilib.h /usr/local/include/
##posibbly add new line removing src below
sudo mkdir /usr/local/src/include
sudo mkdir /usr/local/src/include/linux
sudo ln -s /usr/local/include/comedi.h /usr/local/src/include/linux/comedi.h
sudo ln -s /usr/local/include/comedilib.h /usr/local/src/include/linux/comedilib.h
cd /usr/src/rtai
cd /usr/src/rtai && sudo make menuconfig
##ADD Real Time COMEDI support to RTAI!
##USE DIRECTORY: /usr/local/src/comedi
cd /usr/src/rtai && sudo make
cd /usr/src/rtai && sudo make install
sudo cp /usr/local/src/comedilib/include/comedilib.h /usr/local/include/
###END RTAI #2

##INSERT COMEDI MODULES
cd ~ && cat <<EOF> load_daq
# changes made to do loading comedi modules
sudo modprobe comedi
sudo modprobe kcomedilib
sudo modprobe comedi_fc
#USE PROPER MODULE BELOW!
sudo modprobe ni_mio_cs
sudo insmod /usr/realtime-2.6.32-122-rtai/modules/rtai_comedi.ko
#CONFIGURE DAQ MODULE BELOW!
comedi_config -v /dev/comedi0 ni_mio_cs 0x0100
comedi_calibrate --calibrate
sudo chmod a+rw /dev/comedi0
# end comedi modules setup
EOF
sudo mv ~/load_daq /usr/local/bin/load_daq
sudo chown root:root /usr/local/bin/load_daq
sudo chmod 755 /usr/local/bin/load_daq
sudo chmod a+x /usr/local/bin/load_daq
#cd /usr/local/bin/ && sudo ./load_daq
#If there is a error message "comedi.o not loaded", then you have to create and edit /etc/modprobe.d/comedi and add one of the following #lines:
#1. Legacy Devices:
#options comedi comedi_num_legacy_minors=4
#2. Autoconfigured Devices:
#options comedi comedi_autoconfig=0
##END INSERT COMEDI MODULES

###SCICOSLAB 4.4.1
##BINARY INSTALL PREREQUISITES
cd /opt && sudo wget http://cermics.enpc.fr/~jpc/scilab-gtk-tiddly/files/scicoslab-gtk_4.4.1-1_i386.lucid.deb
cd /opt && sudo wget http://cermics.enpc.fr/~jpc/scilab-gtk-tiddly/files/tkdnd1_1.0-1_i386.lucid.deb
cd /opt && sudo dpkg -i scicoslab-gtk_4.4.1-1_i386.lucid.deb
cd /opt && sudo dpkg -i tkdnd1_1.0-1_i386.lucid.deb
##END BINARY INSTALL

##INSTALL FROM SOURCE
#cd /usr/local/src && sudo wget http://cermics.enpc.fr/~jpc/scilab-gtk-tiddly/files/scicoslab-4.4.1.tgz
#cd /usr/local/src && sudo tar xvzf scicoslab-4.4.1.tgz
#cd /usr/local/src/scicoslab && sudo wget http://hart.sourceforge.net/scilablibs.tar.gz
#cd /usr/local/src/scicoslab && sudo tar xvzf scilablibs.tar.gz
#cd /usr/local/src/scicoslab && sudo sh autogen.sh && sudo make all
#sudo ln -s /opt/scicoslab/bin/scilab /usr/local/bin/scilab
##END INSTALLFROM SOURCE

##SOURCE INSTALL SPECTRUM ANALYZER
cd /usr/local/src && sudo wget http://www.scicos.org/ScicosModNum/modnum_web/files/modnum_422/modnum_422_src.tar.gz
cd /usr/local/src && sudo tar xvzf modnum_422_src.tar.gz
echo "You must run in scicoslab: exec ('builder.sce');"
cd /usr/local/src/modnum_422
scicoslab
#sleep 600
sudo ln -s /usr/local/src/modnum_422 /usr/lib/scicoslab-gtk-4.4.1/contrib/modnum
##END SOURCE INSTALL SPECTRUM ANALYZER
###END SCICOSLAB 4.4.1

##QRTAILab (www.qrtailab.sf.net)
cd /usr/local/src && sudo wget http://downloads.sourceforge.net/qrtailab/QRtaiLab-0.1.12.tar.gz
cd /usr/local/src && sudo tar xvzf QRtaiLab-0.1.12.tar.gz
cd /usr/local/src/qrtailab-0.1.12
##END QRTAILab

##HART TOOLBOX
sudo mkdir /usr/local/src/hart
cd /usr/local/src/hart && sudo wget http://dfn.dl.sourceforge.net/sourceforge/hart/hart_0.4.1-src.tar.gz
sleep 3
cd /usr/local/src/hart && sudo tar xvzf hart_0.4.1-src.tar.gz
#Go in your hart-directory.
cd /usr/local/src/hart/hart_0.4.1
#Edit Makefile and set 'SCILAB = scicoslab' to the binary file of scilab 4.4.1
cd /usr/local/src/hart/hart_0.4.1 && sed -i 's/SCILAB = scilab/SCILAB = scicoslab/g' /usr/local/src/hart/hart_0.4.1/Makefile && sudo su && make && exit
sudo ln -s /usr/local/src/hart/hart_0.4.1 /usr/lib/scicoslab-gtk-4.4.1/contrib/hart
##END HART TOOLBOX

sudo reboot

#EMC2 SHOULD BE PLACED INSIDE ANOTHER SCRIPT AND INVOKED AFTER REBOOT
#sudo mkdir /usr/local/src/emc
#cd /usr/local/src/emc && sudo git clone git://git.linuxcnc.org/git/emc2.git emc2-dev
#cd /usr/local/src/emc/emc2-dev/src && sudo sh autogen.sh && sudo ./configure
#cd /usr/local/src/emc/emc2-dev/src && sudo make && sudo make setuid
#sudo su && . ./usr/local/src/emc/emc2-dev/scripts/emc-environment && exit
##END EMC2
